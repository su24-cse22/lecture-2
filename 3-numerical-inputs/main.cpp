#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that allows the user to input
        grocery product information.

        Example:

        Please input item name:
        Please input item quantity:
        Please input item price:

        Name:     Apples
        Quantity: 25
        Price:    $1.25

    */

    string name;
    int quantity;
    float price;

    cout << "Please input item name: ";
    getline(cin, name);

    cout << "Please input item quantity: ";
    cin >> quantity;

    cout << "Please input item price: ";
    cin >> price;

    cout << endl;
    cout << "Name:     " << name << endl;
    cout << "Quantity: " << quantity << endl;
    cout << "Price:    $" << price << endl;


    return 0;
}