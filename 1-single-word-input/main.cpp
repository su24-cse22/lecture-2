#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that allows the user to input
        their credentials to register for an account.

        Example:

        Please input your email:
        Please input your password:

        Email:    rbobcat@ucmerced.edu
        Password: abc123

    */    

    string email;
    string password;

    cout << "Please input your email: ";
    cin >> email;

    cout << "Please input your password: ";
    cin >> password;

    cout << endl;

    cout << "Email:    " << email << endl;
    cout << "Password: " << password << endl;

    return 0;
}