#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that takes the name, symbol,
        atomic number, atomic mass, and chemical group of
        an element from the periodic table, as input, and
        prints the information.

        Example:

        Name:           Carbon
        Symbol:         C
        Atomic Number:  6
        Atomic Mass:    12.011
        Chemical Group: Nonmetal

    */

    string name, symbol, chemicalGroup;
    int atomicNumber;
    float atomicMass;

    cin >> name;
    cin >> symbol;
    cin >> atomicNumber;
    cin >> atomicMass;
    cin.ignore();
    getline(cin, chemicalGroup);

    cout << "Name:           " << name << endl;
    cout << "Symbol:         " << symbol << endl;
    cout << "Atomic Number:  " << atomicNumber << endl;
    cout << "Atomic Mass:    " << atomicMass << endl;
    cout << "Chemical Group: " << chemicalGroup << endl;

    // How to run program and use input from a file instead of the keyboard
    // ./app < elements/carbon.txt

    return 0;
}