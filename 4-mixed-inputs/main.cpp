#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that allows the user to input
        their personal information and home address.

        Example:

        Please input your name:
        Please input your age:
        Please input your address:
        Please input your city:
        Please input your state:
        Please input your zip code:

        Name: Rufus Bobcat
        Age: 18
        Address: 5200 Lake Road
        City: Merced
        State: California
        Zip Code: 95215
        
    */

    string name, address, city, state, zipCode;
    int age;

    cout << "Please input your name: ";
    getline(cin, name);

    cout << "Please input your age: ";
    cin >> age;

    // clear the input buffer so that getline works correctly
    cin.ignore();

    cout << "Please input your address: ";
    getline(cin, address);

    cout << "Please input your city: ";
    getline(cin, city);

    cout << "Please input your state: ";
    getline(cin, state);

    cout << "Please input your zip code: ";
    getline(cin, zipCode);

    cout << endl;
    cout << "Name:     " << name << endl;
    cout << "Age:      " << age << endl;
    cout << "Address:  " << address << endl;
    cout << "City:     " << city << endl;
    cout << "State:    " << state << endl;
    cout << "Zip Code: " << zipCode << endl;

    return 0;
}