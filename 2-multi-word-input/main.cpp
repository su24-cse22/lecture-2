#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that allows the user to input
        personal information for their social media account.
        
        Please input your name:
        Please input the city you live in:
        Please input your workplace:

        Name:      Rufus Bobcat
        City:      Merced
        Workplace: UC Merced

    */

    string name;
    string city;
    string workplace;

    cout << "Please input your name: ";
    getline(cin, name);

    cout << "Please input the city you live in: ";
    getline(cin, city);

    cout << "Please input your workplace: ";
    getline(cin, workplace);

    cout << endl;
    cout << "Name:      " << name << endl;
    cout << "City:      " << city << endl;
    cout << "Workplace: " << workplace << endl;

    return 0;
}